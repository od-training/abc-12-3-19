import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Video } from '../types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {
  @Input() videoList: Video[] = [];
  @Output() videoSelected = new EventEmitter<Video>();

  selected: Video | undefined;

  constructor() {}

  ngOnInit() {}

  setSelected(video: Video) {
    this.selected = video;
    this.videoSelected.emit(video);
  }
}
