import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Video } from '../types';
import { VideoLoaderService } from '../../video-loader.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent implements OnInit {
  videos: Observable<Video[]>;
  currentVideo: Video | undefined;

  constructor(vls: VideoLoaderService) {
    this.videos = vls
      .getVideos()
      .pipe(map(videoList => transformVideos(videoList)));
  }

  ngOnInit() {}

  selectVideo(video: Video) {
    this.currentVideo = video;
  }
}

function transformVideos(videos: Video[]) {
  return videos.map(video => ({
    ...video,
    title: video.title.toUpperCase()
  }));

  // Object.apply({}, [video, { title: video.title.toUpperCase() }])
}
