import { Component, OnInit, Input } from '@angular/core';

import { Video } from '../types';

@Component({
  selector: 'app-video-thumbnail',
  templateUrl: './video-thumbnail.component.html',
  styleUrls: ['./video-thumbnail.component.css']
})
export class VideoThumbnailComponent implements OnInit {
  @Input('videoDetails') video: Video | undefined;

  constructor() {
    console.log('in constructor:', this.video);
  }

  ngOnInit() {
    console.log('in onInit:', this.video);
  }
}
