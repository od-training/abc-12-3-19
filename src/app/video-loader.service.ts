import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Video } from './dashboard/types';

const apiUrl = 'https://api.angularbootcamp.com';

@Injectable({
  providedIn: 'root'
})
export class VideoLoaderService {
  constructor(private http: HttpClient) {}

  getVideos() {
    return this.http.get<Video[]>(apiUrl + '/videos');
  }
}
